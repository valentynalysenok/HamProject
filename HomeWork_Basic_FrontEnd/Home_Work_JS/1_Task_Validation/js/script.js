function checkAgeValue(age) {
    while (isNaN(age)
        || Number(age) <= 0
        || !Number.isInteger(Number(age))
        ) {
        if (isNaN(age)) {
            age = prompt('Please enter a number value.');
        } else if (Number(age) <= 0) {
            age = prompt('Age value should be greater than 0.');
        } else if (!Number.isInteger(Number(age))) {
            age = prompt("Age value couldn't be a float number.");
        }
    }
    return Number(age);
}

function checkNameValue(name) {
    while (name === null
    || name === ""
    || !isNaN(name)) {
        name = prompt('Please using alphabetical values for name statement.');
    }
    return name;
}


function confirmAgeName(age, name) {
    if (Number(age) < 18) {
        alert('You are not allowed to visit this website.');
    } else if (Number(age) >= 18 && Number(age) <= 22) {
        let confirmValue = confirm('Are you sure you want to continue?');
        if (confirmValue == 1) {
           return alert('Welcome, ' + name);
        } else {
            alert('You are not allowed to visit this website.');
        }
    } else {
        alert('Welcome, ' + name);
    }
}

let age = checkAgeValue(prompt('How old are you?', ''));
let name = checkNameValue(prompt('What is your name?', ''));
confirmAgeName(age, name);

