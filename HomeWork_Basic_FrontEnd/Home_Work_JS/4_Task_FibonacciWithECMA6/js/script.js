/*function getFibonacci(num1, num2, n) {
    let num3;
    for (let i = 0; i < n; i++) {
        num3 = num1 + num2;
        num1 = num2;
        num2 = num3;
    }
    return num3;
}

console.log(getFibonacci(10, 15, 3));

//ES5
let getFibonacci5 = function (num1, num2, n) {
    let num3;
    for (let i = 0; i < n; i++) {
        num3 = num1 + num2;
        num1 = num2;
        num2 = num3;
    }
    return num3;
}

console.log(getFibonacci5(10, 15, 3));*/

//ES6
let getFibonacci6 = (num1, num2, n) => {
    let num3;
    for (let i = 0; i < n; i++) {
        num3 = num1 + num2;
        num1 = num2;
        num2 = num3;
    }
    // noinspection JSAnnotator
    return num3;
};

console.log(getFibonacci6(10, 15, 3));