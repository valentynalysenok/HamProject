function filterCollection(arrayOfObjects, questions, allEntrances) {
    let result = [];
    let args = [];

    for (let j = 0; j < arguments.length; j++) { //перебираем элементы массива с помощью встроеного свойства arguments
        if (j < 3) {
            continue;
        }
        args.push(arguments[j]);
    }
    /*    console.log(args); (5) ["name", "description", "contentType.name", "locales.name", "locales.description"]*/

    for (let i = 0; i < arrayOfObjects.length; i++) {
        if (args.length > 0) {
            for (let j = 0; j < args.length; j++) {

                let path = args[j].split('.');
                let obj = arrayOfObjects[i];
                let flag = false;

                for (let k = 0; k < path.length; k++) { // перебираем массив эл. c, d, e, f, g - arguments (let args = [arguments];)
                    if (typeof(obj) != "object") { //выделили один элемент массива типа => obj = Obj.nnnn
                        flag = true;
                        break;
                    }
                    obj = obj[path[k]];
                    console.log(obj);
                }

                if (flag) {
                    continue;
                }

                rez = search(arrayOfObjects[i], questions, allEntrances);
                if (rez) {
                    break;
                }
            }
        } else {
            rez = search(arrayOfObjects[i], questions, allEntrances);
        }
        if (rez) {
            result.push(arrayOfObjects[i]);
        }
    }
    return result;
}


function search(searchInObject, questions, allEntrances) {
    if (typeof(searchInObject) == 'object' || Array.isArray(searchInObject)) {
        for (let j in searchInObject) {
            let rez = search(searchInObject[j], questions, allEntrances);
            if (rez) {
                return true;
            }
        }
    } else {
        questions = questions.split(' ');
        let q, v;

        v = searchInObject.toLowerCase();
        let countOfEntrances = 0;
        for (let i = 0; i < questions.length; i++) {
            q = questions[i].toLowerCase();
            if (v.indexOf(q) >= 0) {
                countOfEntrances++;
            }
        }
        if ((allEntrances === true && countOfEntrances == questions.length)
            || (allEntrances === false && countOfEntrances > 0)
        ) {
            return true;
        } else {
            return false;
        }
    }

    return false;
}

let searchInObject = [{
    'locales': {
        'name': {
            0: 'asd',
            1: 'en_USasdasdad ToyotA',
            2: 'ToyotA',
        },
        'description': {
            0: 'asd',
            1: 'en_USasdasdad ToyotA',
            2: 'ToyotA',
        }
    },
    'contentType': {
        'name': {
            0: 'asd',
            1: 'en_USasdasdad ToyotA',
            2: 'ToyotA',
        },
        'description': {
            0: 'asd',
            1: 'en_USasdasdad ToyotA',
            2: 'ToyotA',
        }
    },
    0: 'aa aa',
    1: 'b bbb',
    2: 'cc cc',
    3: 'ddd dd',
    'name': 'en_USasdasdad ToyotA',
    'description': 'HONDA fasdsasfaf',
    6: 'asfasdad MAZDA',
    7: 'ToyotA asdadsad',
    8: 'dasdsads VW',
    9: 'lIncilno asdadasd'
}, {
    'locales': {
        'name': {
            0: 'asd',
            1: 'aaa aa',
            2: 'a',
        },
        'description': {
            0: 'asd',
            1: 'a a',
            2: 'a',
        }
    },
    'contentType': {
        'name': {
            0: 'asd',
            1: 'a a',
            2: 'a',
        },
        'description': {
            0: 'asd',
            1: 'a a',
            2: 'a',
        }
    },
    0: 'aa aa',
    1: 'b bbb',
    2: 'cc cc',
    3: 'ddd dd',
    'name': 'a a',
    'description': 'a fasdsasfaf',
    6: 'asfasdad MAZDA',
    7: 'ToyotA asdadsad',
    8: 'dasdsads VW',
    9: 'lIncilno asdadasd'
}, {
    'locales': {
        'name': {
            0: 'asd',
            1: 'en_USasdasdad ToyotA',
            2: 'ToyotA',
        },
        'description': {
            0: 'asd',
            1: 'en_USasdasdad ToyotA',
            2: 'ToyotA',
        }
    },
    'contentType': {
        'name': {
            0: 'asd',
            1: 'en_USasdasdad ToyotA',
            2: 'ToyotA',
        },
        'description': {
            0: 'asd',

            1: 'en_USasdasdad ToyotA',
            2: 'ToyotA',
        },
        'contentType': {
            'name': {
                0: 'asd',
                1: 'en_USasdasdad ToyotA',
                2: 'ToyotA',
            }
        },
        'description': {
            0: 'asd',

            1: 'en_USasdasdad ToyotA',
            2: 'ToyotA',
        }
    },
    0: 'aa aa',
    1: 'b bbb',
    2: 'cc cc',
    3: 'ddd dd',
    'name': 'en_USasdasdad ToyotA',
    'description': 'HONDA fasdsasfaf',
    6: 'asfasdad MAZDA',
    7: 'ToyotA asdadsad',
    8: 'dasdsads VW',
    9: 'lIncilno asdadasd'
}
];
let questions = 'en_US Toyota';
let allEntrances = true; // return Array with 2 elements
/*let allEntrances = false; //return Array with 3 elements*/

let c = 'name';
let d = 'description';
let e = 'contentType.name';
let f = 'locales.name';
let g = 'locales.description';

console.log(filterCollection(searchInObject, questions, allEntrances, c, d, e, f, g));



























