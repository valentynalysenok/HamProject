function filterCollection(vehicles, questions, allEntrances, c, d, e, f, g) {
    //переменные для проверки наличия слова
    let result = {};
    let countOfEntrances;

    //добавляем рекурсию//проверка многомерности массива
    if (typeof(vehicles) == 'object' || Array.isArray(vehicles)) {
        for (let j in vehicles) {
            let rez = filterCollection(vehicles[j], questions, allEntrances, c, d, e, f, g);//рекурсия
            if (rez) {
                result[j] = rez; //будет работать до тех пор, пока не появится строка и эл obj не исчезнет
            }
        }
    } else { //работа со строкой или одномерным массивом
        /*questions[0] = 'b';
        vehicles = 'bbbbbb';*/
        questions = questions.split(' ');
        let q, v;
        v = vehicles.toLowerCase();        //value obj
        countOfEntrances = 0; //к-во вхождений
        for (let i = 0; i < questions.length; i++) {
            q = questions[i].toLowerCase();
            if (v.indexOf(q) >= 0) {              //если совпадение символа vehicles и questions
                countOfEntrances++;               //включаем счетчик
            }
        }
        if ((allEntrances === true && countOfEntrances == questions.length) ||
            (allEntrances === false && countOfEntrances > 0)) {
            return vehicles;
        } else {
            return false;
        }
    }
    /*for (let j in vehicles) {
        v = vehicles[j].toLowerCase(); //value obj
        let countOfEntrances = 0; //к-во вхождений
        for (let i = 0; i < questions.length; i++) {
            q = questions[i].toLowerCase();
            if (v.indexOf(q) >= 0) { //если совпадение символа vehicles и questions
                countOfEntrances++; //включаем счетчик
            }
        }
        if ((allEntrances === true && countOfEntrances == questions.length) ||
            (allEntrances === false && countOfEntrances > 0)) {
            result[j] = vehicles[j];
        }
    }
    return result;*/
    return result;
}


let vehicles = {
    0: 'aa aa',
    1: 'b bbb',
    2: 'cc cc',
    3: 'ddd dd',
    4: 'en_USasdasdad ToyotA',
    5: 'HONDA fasdsasfaf',
    6: 'asfasdad MAZDA',
    7: 'ToyotA asdadsad',
    8: 'dasdsads VW',
    9: 'lIncilno asdadasd',
    'bbbb': {
        'ccccc': {
            0: 'asd',
            1: 'en_USasdasdasd ToyotA',
            2: 'ToyotA',
        }
    }
};

let questions = 'en_US Toyota';
let allEntrances = true; //2 элемента - точное совпадение
/*let allEntrances = false; // хотя */

let c = 'name';
let d = 'description';
let e = 'contentType.name';
let f = 'locales.name';
let g = 'locales.description';

console.log(filterCollection(vehicles, questions, allEntrances, c, d, e, f, g));