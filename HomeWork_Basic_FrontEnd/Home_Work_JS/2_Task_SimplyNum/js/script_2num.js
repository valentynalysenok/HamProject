function checkNumber(num) {
    while (num == null
    || num === ""
    || isNaN(num)
    || !Number.isInteger(Number(num))
    || Number(num) === 1
    || Number(num) <= 0) {
        if (num == null || num === "" || isNaN(num)) {
            num = prompt("Enter a number value.", "");
        } else if (!Number.isInteger(Number(num))) {
            num = prompt("Please don't use float number.", "");
        } else if (Number(num) === 1) {
            num = prompt("Number should be greater than 1.", "");
        } else if (Number(num) <= 0) {
            num = prompt("Please enter a positive number that greater than 0.", "");
        }
    }

    return Number(num);
}

function getSimplyNumbers(num1, num2) {
    let arr = [];

    nextNum:
        for (let i = num1; i <= num2; i++) {

            for (let j = 2; j < i; j++) {
                if (i % j == 0) {
                    continue nextNum;
                }
            }
            arr.push(i);
        }
        return arr;
}


let numSimply1 = checkNumber(prompt('Please enter a first number', ''));
let numSimply2 = checkNumber(prompt('Please enter a second number', ''));
console.log(getSimplyNumbers(numSimply1, numSimply2));
