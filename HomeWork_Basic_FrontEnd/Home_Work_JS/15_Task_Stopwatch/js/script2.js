function formatTime () {
    ++timer;
    hour   = Math.floor(timer / 3600);
    minute = Math.floor((timer - hour * 3600) / 60);
    second = timer - hour * 3600 - minute * 60;
    if (hour < 10) hour = '0' + hour;
    if (minute < 10) minute = '0' + minute;
    if (second < 10) second = '0' + second;
    ms.innerHTML = second;
    sec.innerHTML = minute;
    min.innerHTML = hour;
}

function stopTimer () {
    btnStart.style.display = 'inline';
    btnStop.style.display = 'none';
    clearInterval(discard);
}

function startTimer () {
    btnStart.style.display = 'none';
    btnStop.style.display = 'inline';
    discard = setInterval(formatTime, 13)
}

function deleteTime () {
    timer = 0;                                          // обнуляем общее число прошедшего времени
    clearInterval(discard);                             // обнуляем таймер
    btnStart.style.display = 'inline';
    btnStop.style.display = 'none';
    ms.innerHTML = '00';
    sec.innerHTML = '00';
    min.innerHTML = '00';
}


let btnStart = document.getElementById('startBtn');
let btnStop = document.getElementById('stopBtn');
let btnReset = document.getElementById('resetBtn');
let ms = document.getElementById('ms');
let sec = document.getElementById('sec');
let min = document.getElementById('min');
let discard;
let timer = 0;
let hour = 0;
let minute = 0;
let second = 0;
btnStop.style.display = 'none';
btnStart.onclick = startTimer;
btnStop.onclick = stopTimer;
btnReset.onclick = deleteTime;