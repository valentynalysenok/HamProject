let start = document.querySelector('.start');
let pause = document.querySelector('.pause');
let reset = document.querySelector('.reset');

let mil = document.querySelector('.milis');
let sec = document.querySelector('.secs');
let min = document.querySelector('.mins');

let flag = false;
let timerId;

// hide/display START/PAUSE buttons
function displayStopButton() {
    start.style.display = 'none';
    pause.style.display = 'block';
}

function displayStartButton() {
    start.style.display = 'block';
    pause.style.display = 'none';
}
// Get Date start point
function startStopwatch() {
    if(!flag) initialDate = new Date;
}
//calculate timer
function getTime() {
    let currentDate = new Date();
    timer = new Date(currentDate - initialDate);
    milliseconds = timer.getMilliseconds();
    seconds = timer.getSeconds();
    minutes = timer.getMinutes();
    if(milliseconds < 100){
        milliseconds = '0' + milliseconds;
    }
    if(seconds < 10){
        seconds = '0' + seconds;
    }
    if (minutes < 10){
        minutes = '0' + minutes;
    }
}

//display timer in document
function counter() {
    getTime();
    mil.innerHTML = milliseconds;
    sec.innerHTML = seconds;
    min.innerHTML = minutes;
}

//interval for display
function displayTimer() {
    timerId = setInterval(counter, 10);
}

function stopTimer() {
    clearInterval(timerId);
    getTime();
    flag = true;
}

function resetTimer() {
    flag = false;
    clearInterval(timerId);
    mil.innerHTML = '00';
    min.innerHTML = '00';
    sec.innerHTML = '00';
}

start.addEventListener('click', startStopwatch);
start.addEventListener('click', displayStopButton);
start.addEventListener('click', displayTimer);

pause.addEventListener('click', stopTimer);
pause.addEventListener('click', displayStartButton);

reset.addEventListener('click', resetTimer);
