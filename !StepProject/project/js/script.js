$(function () {

    $("#wr-tabs").on("click", ".tab", function () {

        var tabs = $("#wr-tabs .tab"),
            cont = $("#wr-tabs .tab-cont");

        // Удаляем классы active
        tabs.removeClass("active");
        cont.removeClass("active");
        // Добавляем классы active
        $(this).addClass("active");
        cont.eq($(this).index()).addClass("active");
        return false;
    });
});


$(function () {
    $(".company-work-example-img").slice(0, 12).show();
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $(".company-work-example-img:hidden").slice(0, 12).slideDown();
        if ($(".company-work-example-img:hidden").length == 0) {
            $("#loadMore").fadeOut('slow');
        };
    });
});

$(function () {
    $(".company-best-img-load-more").slice(0, 0).show();
    $("#loadMoreAgain").on('click', function (e) {
        e.preventDefault();
        $(".company-best-img-load-more:hidden").slice(0, 9).slideDown();
        if ($(".company-best-img-load-more:hidden").length == 0) {
            $("#loadMoreAgain").fadeOut('slow');
        }
    });
});


$('.exmpl-img-list-items').click(function () {
    $('.exmpl-img-list-items').removeClass('exmpl-active');
    $(this).addClass('exmpl-active');
    let className = $(this).attr('data-rel');
    $('.company-work-example-img').hide();
    $('.' + className).slice(0, 13).show();
    if (className == "all") {
        $('#loadMore').show();
    } else if (className != "all") {
        $('#loadMore').hide();
    };
});
