/*-----------------Our services-------------------*/
$(function ourServices() {

    $("#wr-tabs").on("click", ".tab", function () {

        let tabs = $("#wr-tabs .tab"),
            cont = $("#wr-tabs .tab-cont");

        // Удаляем классы active
        tabs.removeClass("active");
        cont.removeClass("active");
        // Добавляем классы active
        $(this).addClass("active");
        cont.eq($(this).index()).addClass("active");
        return false;
    });
});
/*----------------------------------------------------*/

/*-----------------Our Amazing Work-------------------*/

let a = 12;
let className = ".company-work-example-img";

$(function ourAmazingWorkLoadMore() {
    $("#loadAnimationContainer").hide();
    $(className).slice(0, a).show();
    $("#loadMoreWorkExamples").on('click', function (e) {
        e.preventDefault();
        $("#loadMoreWorkExamples").hide();
        $("#loadAnimationContainer").show();
        setTimeout(timer, 1000);
    });
});

/*-----------------Animation-------------------*/

function timer () {
    $(className + ":hidden").slice(0, a).slideDown();
    if ($(className + ":hidden").length == a) {
        $("#loadMoreWorkExamples").show();
        $("#loadAnimationContainer").hide();
    }
    if ($(className + ":hidden").length == 0) {
        $("#loadAnimationContainer").hide();
    }
};

/*---------------------------------------------*/

$('.exmpl-img-list-items').click(function ourAmazingWorkSwitch() {
    $('.exmpl-img-list-items').removeClass('exmpl-active');
    $(this).addClass('exmpl-active');
    let className = $(this).attr('data-rel');
    $('.company-work-example-img').hide();
    $('.' + className).slice(0, 13).show();
    if (className == "all") {
        $('#loadMoreWorkExamples').show();
    } else if (className != "all") {
        $('#loadMoreWorkExamples').hide();
    };
});
/*----------------------------------------------------*/

/*----------------Gallery of best images--------------*/
$('.masonry').masonry({
    // options
    itemSelector: '.masonry-item',
    fitWidth: true,
    horizontalOrder: true,
});

let z = 9;
let classMore = ".masonry-item";

$(function loadMoreGalleryImg() {
    $(classMore).css('display', 'none');
    $(classMore).slice(0, z).css('display', 'inline-block');
    $(".masonry").css('height', '956px');
    $("#loadMoreGalleryImg").on('click', function () {
        $(".masonry").css('height', '1846px');
        $(classMore).slideDown().css('display', 'inline-block');
        if ($(classMore + ":hidden").length == 0) {
            $("#loadMoreGalleryImg").fadeOut('slow');
        }
    });
});



/*----------------------------------------------------*/


