/*document.addEventListener('click', function(e){
    if (e.target.closest('.toggle-navbar')) {
        document.getElementsByClassName('header__navbar-nav')[0].classList.toggle('active')
    }
});*/

let menu = document.getElementById("menu");

document.querySelector(".toggle-navbar").onclick = function(e) {
    this.classList.toggle("active");
    if (this.classList.contains("active")) {
        menu.style.display = "flex";
    } else {
        menu.style.display = "none";
    }
}